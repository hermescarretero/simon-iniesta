$(document).ready(function () {
    'use strict';
    $(".btn-form").click(function () {
        $.ajax({
            type: 'POST',
            url: '/index.php/code.json',
            data: $('#codeform').serialize(),
            success: function (response) {
                if (response && response.success) {
                    var data = response.data;
                    if (data.is_big) {
                        document.location.href = '/cuestionario.html?codes[]=' + data.code;
                    } else {
                        $("#codeModal").modal({
                            remote: '/index.php/modal.html?code=' + data.code
                        });
                    }
                }
            },
            error: function (response) {
                if (response && response.responseText) {
                    $('#codeform-error').find('.alert').text(response.responseText);
                }
                $('#codeform-error').removeClass('hidden');
            }

        });

        return false;
    });

    $(".pack-nav li a").on("click", function () {
        $(".pack-nav li").removeClass("active");
        $(this).parent().addClass("active");

    });

    // Checkbox styles
    $('input[type="checkbox"]').change(function () {
        if ($(this).is(':checked')) {
            $(this).parent(".checkbox").addClass("checked");
        } else {
            $(this).parent(".checkbox").removeClass("checked");
        }
    });

    // radio styles
    $('input[type="radio"]').change(function () {
        $(this).parents('.radio-container').find('div').removeClass("checked");
        if ($(this).is(':checked')) {
            $(this).parent(".radio").addClass("checked");
        } else {
            $(this).parent(".radio").removeClass("checked");
        }
    });
    $('.radio-container').find('div:first').addClass('checked');

});

function submitSecondCode(form) {
    'use strict';
    $.ajax({
        type: 'POST',
        url: '/index.php/secondcode.json',
        data: $(form).serialize(),
        success: function (response) {
            if (response && response.success) {
                var data = response.data;
                document.location.href = '/cuestionario.html?codes[]=' + data.first + '&codes[]=' + data.second;
            }
        },
        error: function (response) {
            if (response && response.responseText) {
                $('.modal-body').html(response.responseText);
            }
        }
    });
    return false;
}

//seguimiento de todos los pdf excepto para los vinculos pdf con class="ga_noseguir .permite excluir pdf por la razon que sea"
$("a[href*='.pdf']:not('[class~=ga_noseguir]')").click(function () {
    _gaq.push(['_trackPageview', '/downloads' + $(this).attr('href')]);
});