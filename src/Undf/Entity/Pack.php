<?php

namespace Undf\Entity;

class Pack extends AbstractEntity
{

    protected $id;
    protected $type;
    protected $size;
    protected $team;

    protected $teamTrans = array(
        'barcelona' => 'FC Barcelona',
        'spain' => 'Selección Española'
    );

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getTeam()
    {
        return $this->team;
    }

    public function setTeam($team)
    {
        $this->team = $team;
    }

    public function getTeamName()
    {
        return $this->teamTrans[$this->team];
    }

}
