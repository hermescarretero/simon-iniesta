<?php

namespace Undf\Entity;

use Undf\Entity\Code;

/**
 * User
 */
class User extends AbstractEntity
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $company;

    /**
     * @var string
     */
    protected $cif;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $surname;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $postalCode;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $telephone;

    /**
     * @var string
     */
    protected $mobile;

    /**
     * @var string
     */
    protected $email;
    protected $codes = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Prueba
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set cif
     *
     * @param string $cif
     * @return Prueba
     */
    public function setCif($cif)
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * Get cif
     *
     * @return string
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Prueba
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Prueba
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Prueba
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Prueba
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = strtoupper($postalCode);

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Prueba
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Prueba
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Prueba
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Prueba
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function addCode(Code $code)
    {
        $code->setUserId($this->id);
        $this->codes[] = $code;
    }

    public function removeCode(Code $code)
    {
        $key = array_search($code, $this->codes);
        unset($this->codes[$key]);
    }

    public function getCodes()
    {
        return $this->codes;
    }

    public function setCodes($codes)
    {
        if ($codes instanceof Code) {
            $codes = array($codes);
        } elseif (!is_array($codes)) {
            throw new \Exception("Argument 1 passed to Undf\Entity\User::setCodes() must be an array or an instance of Undf\Entity\Code.");
        }
        $this->codes = $codes;
    }

    public function isAndorran()
    {
        return preg_match('/^(AD)([1-7]{1})(00)/', $this->postalCode);
    }

    public function validate($validateCif = true)
    {
        $filled = $this->name &&
                $this->surname &&
                $this->company &&
                $this->address &&
                $this->postalCode &&
                $this->telephone &&
                $this->email;
        if (!$filled) {
            throw new \Exception('Rellene todos los campos obligatorios.');
        }
        if ($validateCif && !$this->validateCif()) {
            throw new \Exception('El CIF/DNI introducido no es valido.');
        }
        if (!preg_match('/^[^0-9]([a-zA-Z0-9_]+|[.][a-zA-Z0-9_]+)([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*([-][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $this->email)) {
            throw new \Exception('El email introducido no es valido.');
        }
        if (!preg_match("/(^0[1-9]{1}|^[1-4]{1}[0-9]{1}|^5[0-2]{1})([0-9]{3})$/", $this->postalCode) && !$this->isAndorran()) {
            throw new \Exception('El codigo postal introducido no es valido.');
        }
        if (!preg_match("/(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^\(\+[0-9]{2}\)|^00[0-9]{2}|^0|^)([0-9]{9}$|[0-9\-\s]{10}$)/", $this->telephone)) {
            throw new \Exception('El telefono introducido no es valido.');
        }
        if ($this->mobile && !preg_match("/(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^\(\+[0-9]{2}\)|^00[0-9]{2}|^0|^)([0-9]{9}$|[0-9\-\s]{10}$)/", $this->mobile)) {
            throw new \Exception('El movil introducido no es valido.');
        }
    }

    protected function validateCif()
    {
        if($this->isAndorran()) {
            if(preg_match('/^([0-9]{5}|[a-zA-Z]{1}[0-9]{6}[a-zA-Z]{1})$/', $this->cif)) {
                return true;
            }
            throw new \Exception('Introduzca un CIF/DNI valido en Andorra o compruebe el codigo postal');
        }

        //preparación de los datos
        $cifrest = strtoupper($this->cif);
        for ($i = 0; $i < 9; $i++)
            $num[$i] = substr($cifrest, $i, 1);

//si está vacío el campo devuelve error
        if (empty($cifrest)) {
            return false;
        }
//si no tiene un formato valido devuelve error
        elseif (!empty($cifrest) && !ereg('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)', $cifrest)) {
            return false;
        }
//comprobacion de NIFs estandar
        if (ereg('(^[0-9]{8}[A-Z]{1}$)', $cifrest)) {
            if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cifrest, 0, 8) % 23, 1)) {

            } else {
                return false;
            }
        }
//algoritmo para comprobacion de codigos tipo CIF
        $suma = $num[2] + $num[4] + $num[6];
        for ($i = 1; $i < 8; $i += 2)
            $suma += substr((2 * $num[$i]), 0, 1) + substr((2 * $num[$i]), 1, 1);
        $n = 10 - substr($suma, strlen($suma) - 1, 1);
//comprobacion de NIFs especiales (se calculan como CIFs)
        if (ereg('^[KLM]{1}', $cifrest)) {
            if ($num[8] == chr(64 + $n)) {

            } else {
                return false;
            }
        }
//comprobacion de CIFs
        if (ereg('^[ABCDEFGHJNPQRSUVW]{1}', $cifrest)) {
            if ($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1)) {

            } else {
                return false;
            }
        }
//comprobacion de NIEs
//T
        if (ereg('^[T]{1}', $cifrest)) {
            if ($num[8] == ereg('^[T]{1}[A-Z0-9]{8}$', $cifrest)) {

            } else {
                return false;
            }
        }
        return true;
    }

}
