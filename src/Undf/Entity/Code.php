<?php

namespace Undf\Entity;

/**
 * Code
 */
class Code extends AbstractEntity
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var boolean
     */
    protected $is_big;

    /**
     * @var boolean
     */
    protected $test;

    /**
     * @var integer
     */
    protected $user_id;

    /**
     * @var \DateTime
     */
    protected $used_at;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var Undf\Entity\Pack
     */
    protected $pack;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Code
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set is_big
     *
     * @param boolean $isBig
     * @return Code
     */
    public function setIsBig($isBig)
    {
        $this->is_big = $isBig;

        return $this;
    }

    /**
     * Get is_big
     *
     * @return boolean
     */
    public function getIsBig()
    {
        return $this->is_big;
    }

    /**
     * Set test
     *
     * @param boolean $test
     * @return Code
     */
    public function setTest($test)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return boolean
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return Code
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set usedAt
     *
     * @param \DateTime $usedAt
     * @return Code
     */
    public function setUsedAt($usedAt)
    {
        $this->usedAt = $usedAt;

        return $this;
    }

    /**
     * Get usedAt
     *
     * @return \DateTime
     */
    public function getUsedAt()
    {
        return $this->usedAt;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Code
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set pack
     *
     * @param Undf\Entity\Pack $pack
     * @return Code
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * Get pack
     *
     * @return Undf\Entity\Pack
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Checks whether the code is available
     * @return boolean
     */
    public function isAvailableCode()
    {
        return is_null($this->user_id);
    }

}
