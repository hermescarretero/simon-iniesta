<?php
namespace Undf\Entity;

use Doctrine\Common\Util\Inflector;
use Symfony\Component\Form\Exception\InvalidPropertyException;

/**
 * Code
 */
class AbstractEntity
{
    protected $properties;

    protected $_new = true;

    public static function loadFromArray($array)
    {
        $class = get_called_class();
        $entity = new $class();
        $entity->_new = false;
        $entity->properties = $array;
        foreach($array as $key => $value) {
            if(property_exists($entity, $key)) {
                $entity->$key = $value;
            }
        }
        return $entity;

    }

    public function getAsArray()
    {
        return $this->properties;
    }

    public function isNew()
    {
        return $this->_new;
    }

}
