<?php

namespace Undf\Repository;

use Doctrine\Common\Util\Inflector;

abstract class AbstractRepository
{

    /**
     * @var Doctrine\DBAL\Connection
     */
    protected $db;

    public function __construct(\Doctrine\DBAL\Connection $db)
    {
        $this->db = $db;
    }

    public function save($entity)
    {
        if($entity->isNew()) {
            return $this->create($entity);
        } else {
            return $this->update($entity);
        }
    }

    final protected function getEntity(array $array)
    {
        $class = $this->getEntityClass();
        return call_user_func(array($class, 'loadFromArray'), $array);
    }

    abstract protected function getEntityClass();
}
