<?php

namespace Undf\Repository;

use Undf\Entity\Pack;

class PackRepository extends AbstractRepository
{

    public function getEntityClass()
    {
        return '\Undf\Entity\Pack';
    }

    public function create(Pack $pack)
    {
        $sql = "INSERT INTO pack (id,type,size,team) VALUES (?, ?, ?, ?)";
        $params = array();
        $params[] = NULL;
        $params[] = $pack->getType();
        $params[] = $pack->getSize();
        $params[] = $pack->getTeam();
        return $this->db->executeUpdate($sql, $params);

    }

    public function update(Code $code)
    {
        $sql = 'UPDATE pack SET type = ?,size = ?,team = ? WHERE id = ?';

        $params = array();
        $params[] = $pack->getType();
        $params[] = $pack->getSize();
        $params[] = $pack->getTeam();
        $params[] = $pack->getId();
        return $this->db->executeUpdate($sql, $params);
    }

}
