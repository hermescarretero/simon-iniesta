<?php

namespace Undf\Repository;

use Undf\Entity\Code;
use Undf\Entity\Pack;
use Undf\Repository\PackRepository;

class CodeRepository extends AbstractRepository
{

    public function getEntityClass()
    {
        return '\Undf\Entity\Code';
    }

    public function findByCode($codes, $checkAvailability = false)
    {
        $codes = is_array($codes) ? $codes : array($codes);

        $codes = implode("','", $codes);
        $sql = "SELECT * FROM codes WHERE code IN ('$codes')";
        if($checkAvailability) {
            $sql .= ' AND codes.user_id IS NULL';
        }
        $codes = $this->db->fetchAll($sql);

        switch (count($codes)) {
            case 0 :
                return null;
            case 1 :
                return $this->getEntity($codes[0]);
            default :
                $entities = array();
                foreach($codes as $code) {
                    $entities[] = $this->getEntity($code);
                }
                return $entities;
        }
    }

    public function create(Code $code)
    {
        $sql = "INSERT INTO codes (id,code,is_big,test,created_at) VALUES (?, ?, ?, ?, ?)";
        $params = array();
        $params[] = $code->getId();
        $params[] = $code->getCode();
        $params[] = $code->getIsBig();
        $params[] = $code->getTest();
        $params[] = $code->getCreatedAt();
        return $this->db->executeUpdate($sql, $params);

    }

    public function update(Code $code)
    {
        $pack_id = $this->savePack($code->getPack());

        $sql = 'UPDATE codes SET code = ?,user_id = ?,used_at = ?,pack_id = ? WHERE id = ?';

        $params = array();
        $params[] = $code->getCode();
        $params[] = $code->getUserId();
        $params[] = $code->getUsedAt() ? $code->getUsedAt() : date('Y-m-d H:i:s');
        $params[] = $pack_id;
        $params[] = $code->getId();
        return $this->db->executeUpdate($sql, $params);
    }

    public function savePack(Pack $pack)
    {
        if($pack->getId()) {
            return $pack->getId();
        }
        $packRepo = new PackRepository($this->db);
        if($packRepo->save($pack)) {
            return $this->db->lastInsertId();
        }
        return null;
    }

}
