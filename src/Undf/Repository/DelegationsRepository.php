<?php

namespace Undf\Repository;

class DelegationsRepository
{

    private static $delegations = array(
        'catalunya' => array(
            'Barcelona' => array('08'),
            'Girona' => array('17', 'AD'),
            'Tarragona' => array('43'),
            'Lleida' => array('25'),
        ),
        'centro' => array(
            'Madrid' => array('28', '05', '16', '19', '40'),
            'Toledo' => array('45', '13'),
        ),
        'levante' => array(
            'Albacete' => array('02'),
            'Alicante' => array('03'),
            'Baleares' => array('07'),
            'Castellón' => array('12'),
            'Murcia' => array('30'),
            'Valencia' => array('46'),
        ),
        'noreste' => array(
            'Aragón' => array('50', '22', '44'),
            'Burgos' => array('09', '42'),
            'Cantabria' => array('39'),
            'La rioja' => array('26'),
            'Navarra' => array('31'),
            'Vizcaya' => array('48', '20', '01'),
        ),
        'noroeste' => array(
            'Asturias' => array('33', '24', '49', '34'),
            'Pontevedra' => array('36', '32'),
            'Salamanca' => array('37'),
            'Valladolid' => array('47'),
            'A Coruña' => array('15', '27'),
        ),
        'sur' => array(
            'Almería' => array('04'),
            'Cádiz' => array('11'),
            'Córdoba' => array('14'),
            'Badajoz' => array('06', '10'),
            'Granada' => array('18', '23'),
            'Las Palmas' => array('35'),
            'Málaga' => array('29'),
            'Tenerife' => array('38'),
            'Sevilla' => array('41', '21'),
        ),
    );

    public static function getZones()
    {
        return array_keys(self::$delegations);
    }

    public static function getZoneDelegations($zone)
    {
        if(isset(self::$delegations[$zone])) {
            return self::$delegations[$zone];
        }
        throw new \Exception(sprintf('Zone "%s" not found.', $zone));
    }

    public static function getPostalPrefixesByZone($zone)
    {
        $postalCodes = array();
        foreach(self::getZoneDelegations($zone) as $delegations) {
            $postalCodes = array_merge($delegations, $postalCodes);
        }
        return $postalCodes;
    }

    public static function getPostalPrefixesByDelegation($delegation)
    {
        foreach(self::$delegations as $zone => $delegationList){
            if(array_key_exists($delegation, $delegationList)) {
                return $delegationList[$delegation];
            }
        }
        throw new \Exception(sprintf('Delegation "%s" not found.', $delegation));
    }

    public static function getZoneAndDelegationByPostalCode($postalCode)
    {
        $postalPrefix = substr($postalCode, 0, 2);
        $foundZone = '';
        $foundDelegation = '';
        foreach(self::$delegations as $zone => $delegations) {
            foreach($delegations as $delegation => $postalPrefixes) {
                if(in_array($postalPrefix, $postalPrefixes)) {
                    $foundZone = $zone;
                    $foundDelegation = $delegation;
                    break(2);
                }
            }
        }
        return array(ucfirst($foundZone), ucfirst($foundDelegation));

    }

    private static $cities = array(
        'catalunya' => array(
            'Barcelona' => '08',
            'Girona' => '17',
            'Tarragona' => '43',
            'Lleida' => '25',
        ),
        'centro' => array(
            'Madrid' => '28',
            'Ávila' => '05',
            'Cuenca' => '16',
            'Guadalajara' => '19',
            'Segovia' => '40',
            'Toledo' => '45',
            'Ciudad Real' => '13',
        ),
        'levante' => array(
            'Albacete' => '02',
            'Alicante' => '03',
            'Baleares' => '07',
            'Castellón' => '12',
            'Murcia' => '30',
            'Valencia' => '46',
        ),
        'noreste' => array(
            'Zaragoza' => '50',
            'Huesca' => '22',
            'Teruel' => '44',
            'Burgos' => '09',
            'Soria' => '42',
            'Cantabria' => '39',
            'La rioja' => '26',
            'Navarra' => '31',
            'Álava' => '01',
            'Guipúzcua' => '20',
            'Vizcaya' => '48',
        ),
        'noroeste' => array(
            'Asturias' => '33',
            'León' => '24',
            'Zamora' => '49',
            'Palencia' => '34',
            'Pontevedra' => '36',
            'Ourense' => '32',
            'Salamanca' => '37',
            'Valladolid' => '47',
            'A Coruña' => '15',
            'Lugo' => '27',
        ),
        'sur' => array(
            'Almería' => '04',
            'Cádiz' => '11',
            'Córdoba' => '14',
            'Cáceres' => '10',
            'Badajoz' => '06',
            'Granada' => '18',
            'Jaén' => '23',
            'Las Palmas' => '35',
            'Málaga' => '29',
            'Tenerife' => '38',
            'Sevilla' => '41',
            'Huelva' => '21',
        ),
    );

}
