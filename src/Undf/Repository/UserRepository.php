<?php

namespace Undf\Repository;

use Undf\Entity\User;
use Undf\Entity\Code;
use Undf\Repository\CodeRepository;
use Undf\Repository\DelegationsRepository;

class UserRepository extends AbstractRepository
{

    public function getEntityClass()
    {
        return '\Undf\Entity\User';
    }

    public function findByCif($cif)
    {
        $sql = "SELECT * FROM user WHERE cif = ?";
        $user = $this->db->fetchAssoc($sql, array($cif));

        return $user ? $this->getEntity($user) : null;
    }

    public function save($entity)
    {
        return $this->create($entity);
    }

    public function create(User $user)
    {
        $sql = "INSERT INTO user (`company`, `cif`, `name`, `surname`, `address`, `postalCode`, `city`, `telephone`, `mobile`, `email` ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $params = array();
        $params[] = $user->getCompany();
        $params[] = $user->getCif();
        $params[] = $user->getName();
        $params[] = $user->getSurname();
        $params[] = $user->getAddress();
        $params[] = $user->getPostalCode();
        $params[] = $user->getCity();
        $params[] = $user->getTelephone();
        $params[] = $user->getMobile();
        $params[] = $user->getEmail();
        $this->db->executeUpdate($sql, $params);
        $userId = $this->db->lastInsertId();
        $this->saveCodes($user->getCodes(), $userId);
        return 1;
    }

    public function update(User $user)
    {
        $sql = 'UPDATE user SET company = ?,cif = ?,name = ?,surname = ?,address = ?,postalCode = ?,city = ?,telephone = ?,mobile = ?,email = ? WHERE id = ?';

        $params = array();
        $params[] = $user->getCompany();
        $params[] = $user->getCif();
        $params[] = $user->getName();
        $params[] = $user->getSurname();
        $params[] = $user->getAddress();
        $params[] = $user->getPostalCode();
        $params[] = $user->getCity();
        $params[] = $user->getTelephone();
        $params[] = $user->getMobile();
        $params[] = $user->getEmail();
        $params[] = $user->getId();
        $this->db->executeUpdate($sql, $params);
        $this->saveCodes($user->getCodes(), $user->getId());
        return 1;
    }

    public function saveCodes($codes, $userId)
    {
        $codesRepo = new CodeRepository($this->db);
        $updatedAt = date('Y-m-d H:i:s');

        if (count($codes)) {
            $pack_id = $codesRepo->savePack($codes[0]->getPack());
        }

        foreach ($codes as $code) {
            $code->setUserId($userId);
            $code->setUsedAt($updatedAt);

            $code->getPack()->setId($pack_id);

            $codesRepo->save($code);
        }
    }

    public function findPositionByUserCif($cif)
    {
        $cif = strtoupper($cif);
        $sql = "SELECT * FROM user u LEFT JOIN codes c ON u.id = c.user_id LEFT JOIN pack p ON c.pack_id = p.id WHERE p.type LIKE 'tshirt' AND c.is_big IS NOT NULL ORDER BY c.used_at ASC";
        $totals = array();
        foreach ($this->db->fetchAll($sql) as $row) {
            $row['cif'] = strtoupper($row['cif']);
            if (!array_key_exists($row['cif'], $totals)) {
                $totals[$row['cif']] = array();
                $totals[$row['cif']]['users'] = array();
                $totals[$row['cif']]['total'] = 0;
            }
            $totals[$row['cif']]['users'][] = $row;
            $totals[$row['cif']]['total']++;
        }
        //Order the array by descendent total
        $cmp_function = function ($a, $b) {
                    if ($a['total'] < $b['total']) {
                        return true;
                    } elseif ($a['total'] == $b['total']) {
                        return $a['users']['0']['used_at'] > $b['users']['0']['used_at'];
                    } else {
                        return false;
                    }
                };
        uasort($totals, $cmp_function);

        $ranking = array();
        $pos = 0;
        foreach ($totals as $user_cif => $info) {
            $pos++;
            $ranking[$pos] = $info;
            if ($user_cif == $cif) {
                $position = $pos;
            }
        }
        switch ($position) {
            case $position > 15:
                $target = 15;
                break;
            case $position <= 10:
                $target = $position - 1;
                break;
            case 0 :
                return array(0, 0, 0);
            default:
                $target = 15;
        }

        $diff = $ranking[$target]['total'] - $ranking[$position]['total'] + 1;

        return array($position, $target, $diff);
    }

    public function findDoubleCodeParticipants($cifFilter = null)
    {
        $sql = "SELECT * FROM user u LEFT JOIN codes c ON u.id = c.user_id LEFT JOIN pack p ON c.pack_id = p.id WHERE p.type LIKE 'tshirt' AND c.is_big IS NULL";
        if(!is_null($cifFilter)) {
            if($cifFilter) {
                $sql .= ' AND u.cif NOT IN ';
            } else {
                $sql .= ' AND u.cif IN ';
            }
            $sql .= '("'.implode('","', $this->getInactiveCifs()).'")';
        }
        return $this->getRanking($this->db->fetchAll($sql));
    }

    public function findSmallCodeParticipants($cifFilter = null)
    {
        $sql = "SELECT * FROM user u LEFT JOIN codes c ON u.id = c.user_id LEFT JOIN pack p ON c.pack_id = p.id WHERE p.type LIKE 'ball'";
        if(!is_null($cifFilter)) {
            if($cifFilter) {
                $sql .= ' AND u.cif NOT IN ';
            } else {
                $sql .= ' AND u.cif IN ';
            }
            $sql .= '("'.implode('","', $this->getInactiveCifs()).'")';
        }
        return $this->getRanking($this->db->fetchAll($sql));
    }

    public function findBigCodeParticipants($cifFilter = null)
    {
        $sql = "SELECT * FROM user u LEFT JOIN codes c ON u.id = c.user_id LEFT JOIN pack p ON c.pack_id = p.id WHERE p.type LIKE 'tshirt' AND c.is_big IS NOT NULL";
        if(!is_null($cifFilter)) {
            if($cifFilter) {
                $sql .= ' AND u.cif NOT IN ';
            } else {
                $sql .= ' AND u.cif IN ';
            }
            $sql .= '("'.implode('","', $this->getInactiveCifs()).'")';
        }
        $sql .= ' ORDER BY c.used_at ASC';
        return $this->getRanking($this->db->fetchAll($sql));
    }

    public function findTabletPromoParticipants()
    {
        $sql = 'SELECT * FROM user WHERE cif IS NULL';
        $table = $this->db->fetchAll($sql);
        return $table;
    }

    public function findBigCodeParticipantsByZone($zone, $cifFilter = null)
    {
        $postalPrefixes = DelegationsRepository::getPostalPrefixesByZone($zone);
        $byZone = array();
        foreach ($this->findBigCodeParticipants($cifFilter) as $cif => $total) {
            $found = false;
            foreach ($total['users'] as $user) {
                if (!$found) {
                    foreach ($postalPrefixes as $postalPrefix) {
                        if (0 === strpos($user['postalCode'], $postalPrefix)) {
                            $found = true;
                        }
                    }
                }
            }
            if ($found) {
                $byZone[$cif] = $total;
            }
        }
        return $byZone;

//        $postalPrefixesWhereStatement = $this->getPostalPrefixWhereStatement($postalPrefixes);
//        $sql = "SELECT * FROM user u LEFT JOIN codes c ON u.id = c.user_id LEFT JOIN pack p ON c.pack_id = p.id".
//               " WHERE p.type LIKE 'tshirt' AND c.is_big IS NOT NULL AND ($postalPrefixesWhereStatement) ORDER BY c.used_at ASC";
//        return $this->getRanking($this->db->fetchAll($sql));
    }

    public function findDoubleCodeParticipantsByZone($zone, $cifFilter = null)
    {
        $postalPrefixes = DelegationsRepository::getPostalPrefixesByZone($zone);
        $byZone = array();
        foreach ($this->findDoubleCodeParticipants($cifFilter) as $cif => $total) {
            $found = false;
            foreach ($total['users'] as $user) {
                if (!$found) {
                    foreach ($postalPrefixes as $postalPrefix) {
                        if (0 === strpos($user['postalCode'], $postalPrefix)) {
                            $found = true;
                        }
                    }
                }
            }
            if ($found) {
                $byZone[$cif] = $total;
            }
        }
        return $byZone;
    }

    public function findSmallCodeParticipantsByZone($zone, $cifFilter = null)
    {
        $postalPrefixes = DelegationsRepository::getPostalPrefixesByZone($zone);
        $byZone = array();
        foreach ($this->findSmallCodeParticipants($cifFilter) as $cif => $total) {
            $found = false;
            foreach ($total['users'] as $user) {
                if (!$found) {
                    foreach ($postalPrefixes as $postalPrefix) {
                        if (0 === strpos($user['postalCode'], $postalPrefix)) {
                            $found = true;
                        }
                    }
                }
            }
            if ($found) {
                $byZone[$cif] = $total;
            }
        }
        return $byZone;
    }

    public function findBigCodeParticipantsByDelegation($delegation, $cifFilter = null)
    {
        $postalPrefixes = DelegationsRepository::getPostalPrefixesByDelegation($delegation);
        $byDelegation = array();
        foreach ($this->findBigCodeParticipants($cifFilter) as $cif => $total) {
            $found = false;
            foreach ($total['users'] as $user) {
                if (!$found) {
                    foreach ($postalPrefixes as $postalPrefix) {
                        if (0 === strpos($user['postalCode'], $postalPrefix)) {
                            $found = true;
                        }
                    }
                }
            }
            if ($found) {
                $byDelegation[$cif] = $total;
            }
        }
        return $byDelegation;
//        $postalPrefixesWhereStatement = $this->getPostalPrefixWhereStatement($postalPrefixes);
//        $sql = "SELECT * FROM user u LEFT JOIN codes c ON u.id = c.user_id LEFT JOIN pack p ON c.pack_id = p.id".
//               " WHERE p.type LIKE 'tshirt' AND c.is_big IS NOT NULL AND ($postalPrefixesWhereStatement) ORDER BY c.used_at ASC";
//        return $this->getRanking($this->db->fetchAll($sql));
    }

    public function findDoubleCodeParticipantsByDelegation($delegation, $cifFilter = null)
    {
        $postalPrefixes = DelegationsRepository::getPostalPrefixesByDelegation($delegation);
        $byDelegation = array();
        foreach ($this->findDoubleCodeParticipants($cifFilter) as $cif => $total) {
            $found = false;
            foreach ($total['users'] as $user) {
                if (!$found) {
                    foreach ($postalPrefixes as $postalPrefix) {
                        if (0 === strpos($user['postalCode'], $postalPrefix)) {
                            $found = true;
                        }
                    }
                }
            }
            if ($found) {
                $byDelegation[$cif] = $total;
            }
        }
        return $byDelegation;
    }

    public function findSmallCodeParticipantsByDelegation($delegation, $cifFilter = null)
    {
        $postalPrefixes = DelegationsRepository::getPostalPrefixesByDelegation($delegation);
        $byDelegation = array();
        foreach ($this->findSmallCodeParticipants($cifFilter) as $cif => $total) {
            $found = false;
            foreach ($total['users'] as $user) {
                if (!$found) {
                    foreach ($postalPrefixes as $postalPrefix) {
                        if (0 === strpos($user['postalCode'], $postalPrefix)) {
                            $found = true;
                        }
                    }
                }
            }
            if ($found) {
                $byDelegation[$cif] = $total;
            }
        }
        return $byDelegation;
    }

//    protected function getPostalPrefixWhereStatement($postalPrefixes)
//    {
//        $where = '';
//        foreach ($postalPrefixes as $postalPrefix) {
//            $where .= "u.postalCode LIKE '$postalPrefix%' OR ";
//        }
//        return rtrim($where, 'OR ');
//    }

    protected function getRanking($list)
    {
        $totals = array();
        foreach ($list as $row) {
            $row['cif'] = strtoupper($row['cif']);
            if (!array_key_exists($row['cif'], $totals)) {
                $totals[$row['cif']] = array();
                $totals[$row['cif']]['users'] = array();
                $totals[$row['cif']]['total'] = 0;
            } else {
                foreach($totals[$row['cif']]['users'] as $key => $existingRow) {
                    if($existingRow['used_at'] == $row['used_at']) {
                        $totals[$row['cif']]['users'][$key]['code2'] = $row['code'];
                        $totals[$row['cif']]['total']++;
                        continue(2);
                    }
                }
            }

            list($zone, $delegation) = DelegationsRepository::getZoneAndDelegationByPostalCode($row['postalCode']);
            $row['zone'] = $zone;
            $row['delegation'] = $delegation;

            $totals[$row['cif']]['users'][] = $row;
            $totals[$row['cif']]['total']++;
        }
        //Order the array by descendent total
        $cmp_function = function ($a, $b) {
                    if ($a['total'] < $b['total']) {
                        return true;
                    } elseif ($a['total'] == $b['total']) {
                        return $a['users']['0']['used_at'] > $b['users']['0']['used_at'];
                    } else {
                        return false;
                    }
                };
        uasort($totals, $cmp_function);
        $position = 1;
        foreach ($totals as $cif => $total) {
            $totals[$cif]['position'] = $position++;
        }
        return $totals;
    }

    private function getInactiveCifs()
    {
        return array(
            'B06161954',
            '53284216R',
            'J72068422',
            'A11053402',
            '76136937Z',
            '52488317V',
            '32055863G',
            'A10025369',
            '32821120M',
            '79326449R',
            '46718061R',
            'B59764274',
            'A13002134',
            'A02165355',
            '38099918G',
            '27468982J',
            '19991338Z',
        );


    }

}
