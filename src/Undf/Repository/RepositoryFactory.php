<?php

namespace Undf\Repository;

class RepositoryFactory
{

    /**
     * @var Doctrine\DBAL\Connection
     */
    protected $db;

    public function __construct(\Doctrine\DBAL\Connection $db)
    {
        $this->db = $db;
    }

    public function get($entityClass)
    {
        $repoClass = __NAMESPACE__ . '\\' . $entityClass . 'Repository';
        if (!class_exists($repoClass)) {
            throw new \Exception("Repository '$repoClass' does not exit.");
        }
        return new $repoClass($this->db);
    }

}
