<?php

namespace Undf\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Undf\Form\CodeType;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("company", "text", array(
            'label' => ''
        ));
        $builder->add("cif", "text", array(
            'label' => ''
        ));
        $builder->add("name", "text", array(
            'label' => ''
        ));
        $builder->add("surname", "text", array(
            'label' => ''
        ));
        $builder->add("address", "text", array(
            'label' => ''
        ));
        $builder->add("postalCode", "text", array(
            'label' => ''
        ));
        $builder->add("city", "text", array(
            'label' => ''
        ));
        $builder->add("telephone", "text", array(
            'label' => ''
        ));
        $builder->add("mobile", "text", array(
            'required' => false
        ));
        $builder->add("email", "email", array(
            'label' => ''
        ));
        $builder->add('codes', 'collection', array(
            'type' => new CodeType(),
            'allow_add' => true,
            'by_reference' => false,
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => '\Undf\Entity\User',
        );
    }

    public function getName()
    {
        return "usertype";
    }

}