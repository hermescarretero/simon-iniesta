<?php
namespace Undf\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Undf\Form\PackType;

class CodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code', 'text');
        $builder->add('pack', new PackType());
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Undf\Entity\Code',
        );
    }

    public function getName()
    {
        return "codetype";
    }
}