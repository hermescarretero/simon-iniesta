<?php
namespace Undf\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("type", 'choice', array(
            'expanded' => true,
            'multiple' => false,
            'choices' => array(
                'tshirt' => 'Camiseta',
                'ball' => 'Balón'
            )
        ));
        $builder->add("team", 'choice', array(
            'expanded' => true,
            'multiple' => false,
            'choices' => array(
                'barcelona' => 'FC Barcelona',
                'spain' => 'Selección Española'
            )
        ));
        $builder->add("size", 'choice', array(
            'expanded' => true,
            'multiple' => false,
            'choices' => array(
                'L' => 'L',
                'XL' => 'XL'
            )
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Undf\Entity\Pack',
        );
    }

    public function getName()
    {
        return "packtype";
    }
}