<?php

namespace Undf\Controller;

use Undf\Repository\RepositoryFactory;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Undf\Form\CodeType;
use Undf\Form\UserType;
use Undf\Entity\Code;
use Undf\Entity\User;
use Undf\Entity\Pack;

class Controller
{

    protected $repoFactory;
    protected $formFactory;
    protected $twig;
    protected $mailer;

    /**
     * @var Silex\Application
     */
    protected $app;

    public function __construct(\Silex\Application $app)
    {
        $this->repoFactory = $app['repository.factory'];
        $this->formFactory = $app['form.factory'];
        $this->twig = $app['twig'];
        $this->mailer = $app['mailer'];
        $this->app = $app;
    }

    public function postCodeAction(Request $request)
    {
        $params = $request->request->all();

        $code = $this->repoFactory
                ->get('Code')
                ->findByCode($params['codetype']['code']);
        if ($code) {
            $form = $this->formFactory->create(new CodeType());
            $form->bind($request);
            if ($form->isValid()) {
                if ($code->isAvailableCode()) {
                    return new JsonResponse(array(
                                'success' => true,
                                'data' => $code->getAsArray()
                            ));
                }
                return new JsonResponse('Codigo ya canjeado', 400);
            }
        }
        return new JsonResponse('Codigo incorrecto', 400);
    }

    public function postSecondCodeAction(Request $request)
    {
        $params = $request->request->all();
        $firstCode = trim($params['first_code']);
        $secondCode = trim($params['codetype']['code']);

        $errorMessage = 'Codigo incorrecto';

        $code = $this->repoFactory
                ->get('Code')
                ->findByCode($secondCode);
        if (!$firstCode) {
            $errorMessage = 'No se ha introducido el primer código. Vuelva a la página inicial e intentelo otra vez.';
        } elseif ($firstCode == $secondCode) {
            $errorMessage = 'Este código ya ha sido introducido.';
        } elseif ($code && !$code->getIsBig()) {
            $form = $this->formFactory->create(new CodeType(), $code);
            $form->bind($request);
            if ($form->isValid()) {
                if ($firstCode != $secondCode && $code->isAvailableCode()) {
                    return new JsonResponse(array(
                                'success' => true,
                                'data' => array(
                                    'first' => $firstCode,
                                    'second' => $secondCode
                                )
                            ));
                }
                $errorMessage = 'Codigo ya canjeado';
            }
        }
        $code = new Code();
        $code->setCode($secondCode);
        $form = $this->formFactory->create(new CodeType(), $code);
        return new Response($this->twig->render('modal-content.twig', array(
                            'codeForm' => $form->createView(),
                            'firstCode' => $firstCode,
                            'error' => $errorMessage
                        )), 400);
    }

    public function postUserAction(Request $request)
    {
        $template = $request->request->get('template');
        $userInfo = $request->request->get('usertype');
        $firstCode = isset($userInfo['codes'][0]['code']) ? $userInfo['codes'][0]['code'] : false;
        $secondCode = isset($userInfo['codes'][1]['code']) ? $userInfo['codes'][1]['code'] : false;
        $errorMessage = false;

        $user = $this
                ->repoFactory
                ->get('User')
                ->findByCif($userInfo['cif']);

        $form = $this->formFactory->create(new UserType(), $user, array('csrf_protection' => false));
        $form->bind($request);
        if ($form->isValid()) {
            $user = $form->getData();
            try {
                $user->validate();

                $validCodes = array();
                foreach ($user->getCodes() as $key => $code) {
                    $found = $this->repoFactory->get('Code')->findByCode($code->getCode(), true);
                    if ($found) {
                        ($key == 0) && $firstCode = $code->getCode();
                        ($key == 1) && $secondCode = $code->getCode();
                        $found->setPack($code->getPack());
                        $validCodes[] = $found;
                    }
                }
                if (count($user->getCodes()) == count($validCodes)) {
                    $user->setCodes($validCodes);
                    $this->repoFactory->get('User')->save($user);
                    $this->sendEmail($user, $firstCode, $secondCode);
                    return $this->twig->render('forms/form_success.twig', array(
                                'user' => $user,
                                'firstCode' => $firstCode,
                                'secondCode' => $secondCode,
                            ));
                }
                $errorMessage = 'Codigo invalido. Por favor, vaya al inicio y vuelva a introducirlo.';
            } catch (\Exception $e) {
                $errorMessage = $e->getMessage();
            }
        } else {
            $errorMessage = $form->getErrorsAsString();
        }
        return $this->twig->render($template, array(
                    'userForm' => $form->createView(),
                    'firstCode' => $firstCode,
                    'secondCode' => $secondCode,
                    'error' => $errorMessage,
                    'template' => $template
                ));
    }

    public function postTabletUserAction(Request $request)
    {
        $template = $request->request->get('template');
        $userInfo = $request->request->get('usertype');
        $errorMessage = false;

        $user = $this
                ->repoFactory
                ->get('User')
                ->findByCif($userInfo['cif']);

        $form = $this->formFactory->create(new UserType(), $user, array('csrf_protection' => false));
        $form->bind($request);
        if ($form->isValid()) {
            $user = $form->getData();
            try {
                $user->validate(false);
                $this->repoFactory->get('User')->save($user);
                return $this->twig->render('forms/form_tablet_success.twig', array(
                            'user' => $user,
                        ));
            } catch (\Exception $e) {
                $errorMessage = $e->getMessage();
            }
        } else {
            $errorMessage = $form->getErrorsAsString();
        }
        return $this->twig->render('form_tablet.twig', array(
                    'userForm' => $form->createView(),
                    'error' => $errorMessage,
                ));
    }

    /**
     * @param User $user
     * @return int The number of recipients who were accepted for delivery.
     */
    protected function sendEmail(User $user, $firstCode, $secondCode)
    {
        $message = $this->twig->render('mail.twig', array(
                            'user' => $user,
                            'firstCode' => $firstCode,
                            'secondCode' => $secondCode,
                        ));

        $headers = array();
        $headers[] = "Content-type: text/html; charset=utf-8";
        $headers[] = "From: no-reply@simon.es";

        return mail($user->getEmail(), 'Simon e Iniesta', $message, implode("\r\n", $headers));
    }

    public function getFormAction(Request $request)
    {
        $codes = $request->query->get('codes');
        $found = $this->repoFactory
                ->get('Code')
                ->findByCode($codes, true);

        $num_codes = count($codes);
        if ($num_codes && $found) {
            $params = array();

            $user = new User();
            if ($num_codes == 1) {
                if ($found->getIsBig()) {
                    $template = 'form_big.twig';
                    $pack = $this->createPack('tshirt');
                } else {
                    $template = 'form_small.twig';
                    $pack = $this->createPack('ball');
                }
                $code = new Code();
                $code->setCode($codes[0]);
                $code->setPack($pack);
                $user->addCode($code);
            } else {
                $template = 'form_double.twig';
                $params['secondCode'] = $codes[1];

                $pack = $this->createPack('tshirt');

                $code = new Code();
                $code->setCode($codes[0]);
                $code->setPack($pack);
                $user->addCode($code);

                $code = new Code();
                $code->setCode($codes[1]);
                $code->setPack($pack);
                $user->addCode($code);
            }

            $params['userForm'] = $this->formFactory->create(new UserType(), $user)->createView();
            $params['firstCode'] = $codes[0];
            $params['error'] = false;
            $params['template'] = $template;
            return $this->twig->render($template, $params);
        }
        return $this->app->redirect('/');
    }

    protected function createPack($type)
    {
        $pack = new Pack();
        if ($type == 'tshirt') {
            $pack->setType('tshirt');
            $pack->setSize('L');
            $pack->setTeam('barcelona');
        } else {
            $pack->setType('ball');
        }
        return $pack;
    }

    public function postRankingAction(Request $request)
    {
        $cif = $request->request->get('cif');

        list($position, $target, $diff) = $this->repoFactory->get('User')->findPositionByUserCif($cif);

        return new Response($this->twig->render('ranking.twig', array(
                            'position' => $position,
                            'target' => $target,
                            'diff' => $diff
                        )));
    }

}