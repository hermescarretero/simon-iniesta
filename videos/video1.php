<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Una experiencia única con Simon y Andrés Iniesta</title>
        <meta name="description" content="Simon te invita a vivir una experiencia única con Andrés Iniesta, por la compra de packs Simon y además obtendrás un regalo seguro: una camiseta de Andrés Iniesta del Barça o la Selección o de una balón oficial Nike">
        <meta name="Keywords" content="Instalador eléctrico, distribuidor de material eléctrico, interruptores simon, enchufes simon, simon27 play, simon 77, simon 500 cima, interruptor simon, enchufe simon, conectividad voz y datos simon 500 cima" >
        <meta name="viewport" content="width=device-width">
        <style>
            body,html {
                height: 100%;
                margin: 0px;
            }

            #video-youtube {
                width: 100% !important;
                height: 100% !important;
            }
        </style>

        <!--[if lt IE 9]>
            <style>
            body {
                margin: auto;
                min-width: 850px;
                max-width: 1000px;
                _width: 900px;
            }
            #main {
                width: 55%;
            }
            #complementary {
                width: 25%;
                *margin-right: -1px; /* rounding error */
            }
            #aside {
                width: 20%;
            }
            #contentinfo {
                clear:both;
            }
            </style>
        <![endif]-->


    </head>
    <body>
        <!-- Google analytics -->
        <script type="text/javascript">
            var _gaq = _gaq || [];

            _gaq.push(['_setAccount', 'UA-23712500-1']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <div id="video-youtube">
        <iframe src="http://player.vimeo.com/video/58958464?title=0&amp;byline=0&amp;portrait=0" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
        </div>
    </body>
</html>