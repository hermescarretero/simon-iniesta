<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Undf\Repository\RepositoryFactory;
use Undf\Controller\Controller;
use Undf\Form\CodeType;
use Undf\Form\UserType;

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\ServiceControllerServiceProvider());

//Register the security provider
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^/admin/',
            'form' => array('login_path' => '/login', 'check_path' => '/admin/login_check', 'default_target_path' => '/admin/lista.html'),
            'logout' => array('logout_path' => '/admin/logout'),
            'users' => array(
                'simoniniesta' => array('ROLE_ADMIN', 'NNWdTa9JRVAR3f7kHrDm5Uql1EPz5fDogKwL2qA7/vxAW5zeZNemtLkPfHDkhnT9oUPos2gZzR7YQnIcpUGvDw=='),
            ),
        ),
    )
));

//Register the database
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'dbname' => 'simoniniesta',
        'host' => 'localhost',
        'user' => 'simoniniesta',
        'password' => 'EilAcyobEik5',
        'charset' => 'utf8',
        'driverOptions' => array(
            1002 => 'SET NAMES utf8'
        )
    )
));
//Register the form service
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallback' => 'es',
));

//Register the twig service
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/src/Undf/Resources/views',
));

//Register the mailing service
$app->register(new Silex\Provider\SwiftmailerServiceProvider(), array(
    'swiftmailer.options' => array(
        'host' => 'smtp.gmail.com',
        'port' => '465',
        'username' => 'developer@undefined.es',
        'password' => 'd3v3lopm3nt',
        'encryption' => 'ssl',
        'auth_mode' => 'login'
    )
));


$app['repository.factory'] = $app->share(function() use ($app) {
            return new RepositoryFactory($app['db']);
        });
$app['controller'] = $app->share(function() use ($app) {
            return new Controller($app);
        });


/**
 * Routing
 */
$app->match('/', function() use ($app) {
            $codeform = $app["form.factory"]->create(new CodeType());

            return $app['twig']->render('home.twig', array(
                        'codeForm' => $codeform->createView(),
                    ));
        });
$app->post('/code.json', "controller:postCodeAction");
$app->post('/secondcode.json', "controller:postSecondCodeAction");
$app->post('/ranking.html', "controller:postRankingAction");

$app->post('/cuestionario.html', "controller:postUserAction");
$app->get('/cuestionario.html', "controller:getFormAction");

$app->get('/cuestionario-tablet.html', function() use ($app) {
            $form = $app['form.factory']->create(new Undf\Form\UserType);
            return $app['twig']->render('form_tablet.twig', array(
                        'userForm' => $form->createView(),
                        'error' => false
                    ));
        });
$app->post('/cuestionario-tablet.html', "controller:postTabletUserAction");

$app->get('/modal.html', function(Request $request) use ($app) {
            $firstCode = $request->query->get('code');
            $codeform = $app["form.factory"]->create(new CodeType());

            return $app['twig']->render('modal-content.twig', array(
                        'codeForm' => $codeform->createView(),
                        'firstCode' => $firstCode,
                        'error' => false
                    ));
        });

$app->get('/bases', function() use ($app) {
            return $app['twig']->render('bases.twig');
        });

$app->get('/bases_balon', function() use ($app) {
            return $app['twig']->render('bases_balon.twig');
        });

$app->get('/bases_tablet', function() use ($app) {
            return $app['twig']->render('bases_tablet.twig');
        });

$app->get('/bases_descubre', function() use ($app) {
            return $app['twig']->render('bases_descubre.twig');
        });

$app->get('/packs', function() use ($app) {
            return $app['twig']->render('packs.twig');
        });

$app->get('/pack_camiseta', function() use ($app) {
            return $app['twig']->render('pack_camiseta.twig');
        });

$app->get('/pack_balon', function() use ($app) {
            return $app['twig']->render('pack_balon.twig');
        });


$app->get('/ranking.html', function() use ($app) {
            return $app['twig']->render('ranking.twig');
        });

$app->get('/login', function(Request $request) use ($app) {
            return $app['twig']->render('login.twig', array(
                        'error' => $app['security.last_error']($request),
                        'last_username' => $app['session']->get('_security.last_username'),
                    ));
        });

$app->get('/admin/lista.html', function() use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findBigCodeParticipants($activeFilter);
            return $app['twig']->render('admin/list-bigcode.twig', array(
                        'table' => $table,
                        'linkToExport' => 'exportar-camiseta-experiencia.php'.(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 1,
                        'activeZone' => 'Todo',
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'activeFilter' => $activeFilter
                    ));
        });
$app->get('/admin/lista.html/{zone}', function($zone) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $delegations = array_keys(Undf\Repository\DelegationsRepository::getZoneDelegations($zone));
            $table = $app['repository.factory']->get('User')->findBigCodeParticipantsByZone($zone, $activeFilter);
            return $app['twig']->render('admin/list-bigcode.twig', array(
                        'table' => $table,
                        'linkToExport' => "/admin/exportar-camiseta-experiencia.php/$zone".(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 1,
                        'activeZone' => $zone,
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'delegations' => $delegations,
                        'activeDelegation' => 'Todo',
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista.html/{zone}/{delegation}', function($zone, $delegation) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $delegations = array_keys(Undf\Repository\DelegationsRepository::getZoneDelegations($zone));
            $table = $app['repository.factory']->get('User')->findBigCodeParticipantsByDelegation($delegation, $activeFilter);
            return $app['twig']->render('admin/list-bigcode.twig', array(
                        'table' => $table,
                        'linkToExport' => "/admin/exportar-camiseta-experiencia.php/$zone/$delegation".(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 1,
                        'activeZone' => $zone,
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'delegations' => $delegations,
                        'activeDelegation' => $delegation,
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-camiseta.html', function() use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findDoubleCodeParticipants($activeFilter);
            return $app['twig']->render('admin/list-doublecode.twig', array(
                        'table' => $table,
                        'linkToExport' => 'exportar-camiseta.php'.(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado dos codigos pequeños',
                        'activeList' => 2,
                        'activeZone' => 'Todo',
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-camiseta.html/{zone}', function($zone) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $delegations = array_keys(Undf\Repository\DelegationsRepository::getZoneDelegations($zone));
            $table = $app['repository.factory']->get('User')->findDoubleCodeParticipantsByZone($zone, $activeFilter);
            return $app['twig']->render('admin/list-doublecode.twig', array(
                        'table' => $table,
                        'linkToExport' => "/admin/exportar-camiseta.php/$zone".(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 2,
                        'activeZone' => $zone,
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'delegations' => $delegations,
                        'activeDelegation' => 'Todo',
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-camiseta.html/{zone}/{delegation}', function($zone, $delegation) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $delegations = array_keys(Undf\Repository\DelegationsRepository::getZoneDelegations($zone));
            $table = $app['repository.factory']->get('User')->findDoubleCodeParticipantsByDelegation($delegation, $activeFilter);
            return $app['twig']->render('admin/list-doublecode.twig', array(
                        'table' => $table,
                        'linkToExport' => "/admin/exportar-camiseta.php/$zone/$delegation".(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 2,
                        'activeZone' => $zone,
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'delegations' => $delegations,
                        'activeDelegation' => $delegation,
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-balon.html', function() use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findSmallCodeParticipants($activeFilter);
            return $app['twig']->render('admin/list-smallcode.twig', array(
                        'table' => $table,
                        'linkToExport' => 'exportar-balon.php'.(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo pequeño',
                        'activeList' => 3,
                        'activeZone' => 'Todo',
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-balon.html/{zone}', function($zone) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $delegations = array_keys(Undf\Repository\DelegationsRepository::getZoneDelegations($zone));
            $table = $app['repository.factory']->get('User')->findSmallCodeParticipantsByZone($zone, $activeFilter);
            return $app['twig']->render('admin/list-smallcode.twig', array(
                        'table' => $table,
                        'linkToExport' => "/admin/exportar-balon.php/$zone".(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 3,
                        'activeZone' => $zone,
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'delegations' => $delegations,
                        'activeDelegation' => 'Todo',
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-balon.html/{zone}/{delegation}', function($zone, $delegation) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $delegations = array_keys(Undf\Repository\DelegationsRepository::getZoneDelegations($zone));
            $table = $app['repository.factory']->get('User')->findSmallCodeParticipantsByDelegation($delegation, $activeFilter);
            return $app['twig']->render('admin/list-smallcode.twig', array(
                        'table' => $table,
                        'linkToExport' => "/admin/exportar-balon.php/$zone/$delegation".(is_null($activeFilter) ? '' : '?active='.$activeFilter),
                        'title' => 'Listado de personas que han canjeado un codigo grande',
                        'activeList' => 3,
                        'activeZone' => $zone,
                        'zones' => Undf\Repository\DelegationsRepository::getZones(),
                        'delegations' => $delegations,
                        'activeDelegation' => $delegation,
                        'activeFilter' => $activeFilter
                    ));
        });

$app->get('/admin/lista-tablet.html', function() use ($app) {
            $table = $app['repository.factory']->get('User')->findTabletPromoParticipants();
            return $app['twig']->render('admin/list-tablet.twig', array(
                        'table' => $table,
                        'linkToExport' => 'exportar-tablet.php',
                        'title' => 'Listado de participantes en la promocion TABLET',
                        'activeList' => 4,
                        'activeFilter' => null
                    ));
        });



$app->get('/admin/exportar-camiseta-experiencia.php', function() use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findBigCodeParticipants($activeFilter);
            $response = $app['twig']->render('admin/export-bigcode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-camiseta-experiencia.php/{zone}', function($zone) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findBigCodeParticipantsByZone($zone, $activeFilter);
            $response = $app['twig']->render('admin/export-bigcode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-camiseta-experiencia.php/{zone}/{delegation}', function($zone, $delegation) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findBigCodeParticipantsByDelegation($delegation, $activeFilter);
            $response = $app['twig']->render('admin/export-bigcode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-camiseta.php', function() use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findDoubleCodeParticipants($activeFilter);
            $response = $app['twig']->render('admin/export-doublecode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-camiseta.php/{zone}', function($zone) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findDoubleCodeParticipantsByZone($zone, $activeFilter);
            $response = $app['twig']->render('admin/export-doublecode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-camiseta.php/{zone}/{delegation}', function($zone, $delegation) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findDoubleCodeParticipantsByDelegation($delegation, $activeFilter);
            $response = $app['twig']->render('admin/export-doublecode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-balon.php', function() use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findSmallCodeParticipants($activeFilter);
            $response = $app['twig']->render('admin/export-smallcode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-balon.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-balon.php/{zone}', function($zone) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findSmallCodeParticipantsByZone($zone, $activeFilter);
            $response = $app['twig']->render('admin/export-smallcode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-balon.php/{zone}/{delegation}', function($zone, $delegation) use ($app) {
            if(null !== $activeFilter = $app['request']->get('active')) {
                $activeFilter = (int)$activeFilter;
            }
            $table = $app['repository.factory']->get('User')->findSmallCodeParticipantsByDelegation($delegation, $activeFilter);
            $response = $app['twig']->render('admin/export-smallcode.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-camiseta-experiencia.csv'
            );
            return new Response($response, 200, $headers);
        });

$app->get('/admin/exportar-tablet.php', function() use ($app) {
            $table = $app['repository.factory']->get('User')->findTabletPromoParticipants();
            $response = $app['twig']->render('admin/export-tablet.twig', array(
                'table' => $table,
                    ));

            $headers = array(
                'Content-Encoding' => 'UTF-8',
                'Content-Type' => 'application/csv; charset=UTF-8',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'content-disposition' => 'attachment;filename=lista-tablet.csv'
            );
            return new Response($response, 200, $headers);
        });



$app->run();
